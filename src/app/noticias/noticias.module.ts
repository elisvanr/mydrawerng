import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { NoticiasRoutingModule } from './noticias-routing.module';
import { NoticiasComponent } from './noticias.component';


@NgModule({
  declarations: [
    NoticiasComponent
  ],
  imports: [
         NativeScriptCommonModule,
         NoticiasRoutingModule

  ],

  schemas: [NO_ERRORS_SCHEMA]
})

export class NoticiasModule { }
