import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import * as app from "tns-core-modules/application";
@Component({
  selector: 'noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.css']
})
export class NoticiasComponent implements OnInit {

  constructor(
      private router : RouterExtensions
  ) { }

  ngOnInit(): void {
      console.log('Entro a noticias');
      this.saberPlataforma();
  }
  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
}

onNavigate(): void {
    this.router.navigate(['/animales']);
}
saberPlataforma(): void {
    if (`${isAndroid}=true`) {
        let plataforma:string = "Ejecutando en Android";
        console.log(`La plataforma en ejecucion ${plataforma}`);
    }

}


}
