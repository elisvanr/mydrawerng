import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouterExtensions } from 'nativescript-angular/router';
import {NoticiasComponent} from './../noticias/noticias.component';

@Component({
  selector: 'Animales',
  templateUrl: './animales.component.html',
  styleUrls: ['./animales.component.css']
})
export class AnimalesComponent implements OnInit {

  constructor(
      private router: RouterExtensions
  ) { }

  ngOnInit(): void {
  }
  onBack(){
      this.router.back();
  }
}
