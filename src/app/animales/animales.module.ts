import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';

import { AnimalesRoutingModule } from './animales-routing.module';
import { AnimalesComponent } from './animales.component';



@NgModule({
  declarations: [
      AnimalesComponent
  ],
  imports: [
    NativeScriptCommonModule,
    AnimalesRoutingModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AnimalesModule { }
